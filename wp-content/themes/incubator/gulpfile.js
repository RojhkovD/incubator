var gulp   = require('gulp'),
    csso   = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    concat_css = require('gulp-concat'),
    prefix = require('gulp-autoprefixer'),
    live = require('gulp-livereload'),
    cssb = require('gulp-cssbeautify');

var files = {
    texts:     ['*.php', '*/**.php'],
    styles:    'css/*.css',
    scripts:   ['js/jquery.js','js/*.js'],
    images:    'img/*.{gif,jpg,jpeg,png,svg}'
};

gulp.task('text', function() {
    gulp.src(files.texts)
        .pipe(live());

});

gulp.task('script', function() {
    gulp.src('js/*.js')
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest('./js'));

});

gulp.task('script-prod', function() {
    gulp.src('js/*.js')
        .pipe(uglify({
            compress: true,
            mangle: true
        }))
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest('./js'));

});

gulp.task('style', function() {

    gulp.src(files.styles)
        .pipe(cssb({
            indent: '  ',
            openbrace: 'separate-line',
            autosemicolon: true
        }))
        .pipe(gulp.dest('./css'))
        .pipe(live());

});


gulp.task('style-prod', function() {

    gulp.src(files.styles)
        .pipe(prefix())
        .pipe(csso())
        .pipe(concat_css('style.min.css'))
        .pipe(gulp.dest('./css'));

});

gulp.task('build', ['style', 'script', 'text']);
gulp.task('build-prod', ['style-prod', 'script-prod']);

gulp.task('default', ['build'], function() {

    live.listen();

    gulp.watch(files.texts,   ['text']);
    gulp.watch(files.styles,  ['style']);
    gulp.watch(files.scripts, ['script']);
});