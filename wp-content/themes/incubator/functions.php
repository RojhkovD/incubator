<?php 
function incubator_setup()  {
    wp_enqueue_style( 'incubator-styles', get_template_directory_uri() . '/css/style.min.css');
    wp_enqueue_script( 'incubator-scripts', get_template_directory_uri() . '/js/main.min.js');
}


function parse_prev($content){

    $t = get_the_title();
    $title = "<p class=\"head2\">"."$t"."</p>";
    $first_pos1 = strpos($content, "<img");
    $first_pos1 = 0;
    $sub1 = substr($content, $first_pos1);
    $first_pos2 = strripos($sub1, "<a href");
    $text = substr($sub1, 0, $first_pos2);

  $first_pos1 = strripos($content, "<a href=");
  $sub1 = substr($content, $first_pos1); 
  $first_pos2 = strpos($sub1, "/#");
  $link = substr($sub1, 0, $first_pos2+1);
  $link = $link."\" >";
  $title = $link.$title."</a>";
  $text = "<div><p style=\"text-align:left;\">".$text."</p></div>";
  $res = array(
    'text' => $text,
    'title' => $title,
    'link' => $link);
  return $res;
}

function register_my_menu() {
  register_nav_menu('Menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

function register_my_menus() {
  register_nav_menus(
    array(
      'Menu' => __( 'Header Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

function business_services() {
  
  $args = array(
    'labels'        => $labels,
    'public'        => true,
    'menu_position' => null,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'business-services', $args ); 
}
add_action( 'init', 'business_services' );

add_theme_support('post-thumbnails');
add_image_size('small-thumbnail', 270, 170, true);
add_image_size('wall-image', 920, 210, true);



function get_paginated_numbers( $args = [] ) {
    if ( $args['query']->max_num_pages === 0){
      return '';
    }
    //Set defaults to use
    $defaults = [
        'query'                 => $GLOBALS['wp_query'],
        'previous_page_text'    => __( '&laquo;' ),
        'next_page_text'        => __( '&raquo;' ),
        'first_page_text'       => __( 'First' ),
        'last_page_text'        => __( 'Last' ),
        'next_link_text'        => __( 'Older Entries' ),
        'previous_link_text'    => __( 'Newer Entries' ),
        'show_posts_links'      => false,
        'range'                 => 5,
    ];

    // Merge default arguments with user set arguments
    $args = wp_parse_args( $args, $defaults );

    /**
     * Get current page if query is paginated and more than one page exists
     * The first page is set to 1
     * 
     * Static front pages is included
     *
     * @see WP_Query pagination parameter 'paged'
     * @link http://codex.wordpress.org/Class_Reference/WP_Query#Pagination_Parameters
     *
    */ 
    if ( get_query_var('paged') ) { 

        $current_page = get_query_var('paged'); 

    }elseif ( get_query_var('page') ) { 

        $current_page = get_query_var('page'); 

    }else{ 

        $current_page = 1; 

    }

    // Get the amount of pages from the query
    $max_pages      = (int) $args['query']->max_num_pages;

    /**
     * If $args['show_posts_links'] is set to false, numbered paginated links are returned
     * If $args['show_posts_links'] is set to true, pagination links are returned
    */
    if( false === $args['show_posts_links'] ) {

        // Don't display links if only one page exists
        if( 1 === $max_pages ) {

            $paginated_text = '';

        }else{

            /**
             * For multi-paged queries, we need to set the variable ranges which will be used to check
             * the current page against and according to that set the correct output for the paginated numbers
            */
            $mid_range      = (int) floor( $args['range'] / 2 );
            $start_range    = range( 1 , $mid_range );
            $end_range      = range( ( $max_pages - $mid_range +1 ) , $max_pages );
            $exclude        = array_merge( $start_range, $end_range );  

            /**
             * The amount of pages must now be checked against $args['range']. If the total amount of pages
             * is less than $args['range'], the numbered links must be returned as is
             *
             * If the total amount of pages is more than $args['range'], then we need to calculate the offset
             * to just return the amount of page numbers specified in $args['range']. This defaults to 5, so at any
             * given instance, there will be 5 page numbers displayed
            */
            $check_range    = ( $args['range'] > $max_pages )   ? true : false;

            if( true === $check_range ) {

                $range_numbers = range( 1, $max_pages );

            }elseif( false === $check_range ) {

                if( !in_array( $current_page, $exclude ) ) {

                    $range_numbers = range( ( $current_page - $mid_range ), ( $current_page + $mid_range ) );

                }elseif( in_array( $current_page, $start_range ) && ( $current_page - $mid_range ) <= 0 ) {

                    $range_numbers = range( 1, $args['range'] );

                }elseif(  in_array( $current_page, $end_range ) && ( $current_page + $mid_range ) >= $max_pages ) {

                    $range_numbers = range( ( $max_pages - $args['range'] +1 ), $max_pages );

                }

            }

            /**
             * The page numbers are set into an array through this foreach loop. The current page, or active page
             * gets the class 'current' assigned to it. All the other pages get the class 'inactive' assigned to it
            */
            foreach ( $range_numbers as $v ) {

                if ( $v == $current_page ) { 

                    $page_numbers[] = '<span class="current">' . "<p class=\"nomargin now\">" . $v . "</p>" . '</span>';

                }else{

                    $page_numbers[] = '<a href="' . get_pagenum_link( $v ) . '" class="inactive">' . "<p class=\"nomargin\">" . $v . "</p>" . '</a>';

                }

            }


            /** 
            * All the texts are set here and when they should be displayed which will link back to:
             * - $previous_page The previous page from the current active page
             * - $next_page The next page from the current active page
             * - $first_page Links back to page number 1
             * - $last_page Links to the last page
            */
           
            $previous_page  = ( $current_page !== 1 )                       ? "<div class=\"pg_ar_prev\"><a class=\"pg_a\" href=\"" . get_pagenum_link( $current_page - 1 ) . '">' . '<img class="pg_prev enabled_ar" src="'. get_template_directory_uri() . "/img/pg_arrow_enabled.png\">" . $args['previous_page_text'] . '</a></div>'
            : "</a><div class=\"pg_ar_prev disabled pg_a\">" . '<img class="pg_prev disabled_ar" src="'. get_template_directory_uri() . "/img/pg_arrow_disabled.png\">" . $args['previous_page_text'] . '</div>';
            $next_page      = ( $current_page !== $max_pages )              ? "<div class=\"pg_ar_next\"><a class=\"pg_a\" href=\"" . get_pagenum_link( $current_page + 1 ) . '">' .  $args['next_page_text'] . '<img class="pg_next enabled_ar" src="'. get_template_directory_uri() . "/img/pg_arrow_enabled.png\">" . '</a></div>'
            : "</a><div class=\"pg_ar_next disabled pg_a\">" . $args['next_page_text'] . '<img class="pg_next disabled_ar" src="'. get_template_directory_uri() . "/img/pg_arrow_disabled.png\">" . '</div>';
            $first_page     = ( !in_array( 1, $range_numbers ) )            ? '<a href="' . get_pagenum_link( 1 ) . '">' . "<p class=\"nomargin\">" . "1" . "</p>" . '</a>' : '';
            $first_page     = ( !in_array( 1, $range_numbers ) && !in_array( 2, $range_numbers ) )            ? '<a href="' . get_pagenum_link( 1 ) . '">' . "<p class=\"nomargin\">" . "1" . "</p>" . '</a>' . '<span style="padding:0 5px;" class=\"nomargin\">...</span>' 
            : $first_page;
            $first_page     = ( in_array( 3, $range_numbers ) && !in_array( 2, $range_numbers ) )            ? '<a href="' . get_pagenum_link( 1 ) . '">' . "<p class=\"nomargin\">" . "1" . "</p>" . '</a>' . '<a href="' . get_pagenum_link( 2 ) . '">' . "<p class=\"nomargin\">" . "2" . "</p>" . '</a>' 
            : $first_page;
            $last_page      = ( !in_array( $max_pages, $range_numbers ) )   ? '<a href="' . get_pagenum_link( $max_pages ) . '">' . "<p class=\"nomargin\">" . $max_pages . "</p>" . '</a>' : '';
            $last_page      = ( !in_array( $max_pages, $range_numbers ) && !in_array( $max_pages - 1, $range_numbers ) )   ? '<span style="padding:0 5px;" class=\"nomargin\">...</span>' .  '<a href="' . get_pagenum_link( $max_pages ) . '">' . "<p class=\"nomargin\">" . $max_pages . "</p>" . '</a>' : $last_page;
            $last_page      = ( in_array( $max_pages - 2, $range_numbers ) && !in_array( $max_pages - 1, $range_numbers ))   ? '<a href="' . get_pagenum_link( $max_pages - 1 ) . '">' . "<p class=\"nomargin\">" . ($max_pages - 1) . "</p>" . '</a>' .  '<a href="' . get_pagenum_link( $max_pages ) . '">' . "<p class=\"nomargin\">" . $max_pages . "</p>" . '</a>' : $last_page;
            /**
             * Text to display before the page numbers
             * This is set to the following structure:
             * - Page X of Y
            */
            // Turn the array of page numbers into a string
            $numbers_string = implode( ' ', $page_numbers );
            // The final output of the function
            $paginated_text = '<div class="pagination">';
            $paginated_text .=  $first_page . $previous_page . $numbers_string . $next_page . $last_page;
            $paginated_text .= '</div>';

        }

    }elseif( true === $args['show_posts_links'] ) {

        /**
        * If $args['show_posts_links'] is set to true, only links to the previous and next pages are displayed
        * The $max_pages parameter is already set by the function to accommodate custom queries
        */
        $paginated_text = next_posts_link( '<div class="next-posts-link">' . $args['next_link_text'] . '</div>', $max_pages );
        $paginated_text .= previous_posts_link( '<div class="previous-posts-link">' . $args['previous_link_text'] . '</div>' );

    }

    // Finally return the output text from the function
    return $paginated_text;

}

function error_show($message){
    echo "<div class=\"error\">". $message ."</div>";
}


function success_show($message){
    echo "<div class=\"success\">". $message ."</div>";
}

function processing($a){
    $a = htmlspecialchars(strip_tags(stripcslashes(trim($a))));
    return $a;
}


function sign_up($user, $email, $company, $phone, $file, $is_rep){
    $allowed_types =  array('gif','png' ,'jpg', 'jpeg');
    if(!empty($user) && !empty($email) && !empty($company) && !empty($phone)){
        $ar = array($user, $email, $company, $phone);
        array_map("processing", $ar);
          if (filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/^[a-zA-Z0-9\p{Cyrillic}]{5,}$/u', $user) 
            && in_array(pathinfo($file, PATHINFO_EXTENSION), $allowed_types) && preg_match("/^[0-9]{7,11}$/", $phone)
            && preg_match('/^[a-zA-Z0-9\s\p{Cyrillic}]{3,}$/u', $company)) {
             if( null == username_exists($user )) {
                $password = wp_generate_password( 8, true );
                if( null == email_exists($email )) { 
                    $userdata = array('user_pass' => $password, //обязательно
                      'user_login' => $user, //обязательно
                      'user_email' => $email,
                      'user_phone' => $phone,
                      'role' => 'candidate' // (строка) роль пользователя
                    );
                    $user_id = wp_insert_user( $userdata );
                    $user_meta = add_user_meta($user_id, 'company', $company);
                    if(is_int($user_id)){
                    $message = "Ваш запрос отправлен";
                    success_show($message);
                    return $user_id;
                    }else{
                    $message = "Ошибка: извините, что то пошло не так, попробуйте еще раз";
                    error_show($message);
                    }
                }else{
                    $message = "Ошибка: извините, указанный адрес электронной почты уже зарегистрирован";
                    error_show($message);
                }
            }else{
                $message = "Ошибка: извините, такой пользователь уже зарегистрирован";
                error_show($message);
            }
        }else{
            $message = "Ошибка: проверьте корректность введенных данных";
            error_show($message);
        }
    }else{
        $message = "Ошибка: необходимо заполнить все поля";
        error_show($message);
    }
}

$result = add_role( 'applicant', __('applicant' ),
  
                              array(
  
                                    'read' => true, // true allows this capability
                                    'edit_posts' => false, // Allows user to edit their own posts
                                    'edit_pages' => false, // Allows user to edit pages
                                    'edit_others_posts' => false, // Allows user to edit others posts not just their own
                                    'create_posts' => false, // Allows user to create new posts
                                    'manage_categories' => false, // Allows user to manage post categories
                                    'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
                                    'edit_themes' => false, // false denies this capability. User can’t edit your theme
                                    'install_plugins' => false, // User cant add new plugins
                                    'update_plugin' => false, // User can’t update any plugins
                                    'update_core' => false, // user cant perform core updates
                                    'upload_files' => true
  )
  
);

function set_avatar($id, $file){
    if(empty($file)){
        if ($id) {
            $user_info = get_userdata($id);
            $id = $user_info->ID;
        }

        if (is_user_logged_in()) {

        if(isset($_POST['user_avatar_edit_submit'])){
             do_action('edit_user_profile_update', $id);
        }
        ?>
        <form action="<?php the_permalink();?>" method="post" id="avatar-upload-form" class="standard-form" enctype="multipart/form-data">;      
        <?php
        $myAv = new simple_local_avatars();
        $myAv->edit_user_profile( get_userdata($id) );
        
        echo '<p id="avatar-upload">        
        <input type="submit" name="upload" id="upload" value="Submit" />
        <input type="hidden" name="user_avatar_edit_submit" value="OK"/>
        </p>
        </form>';

        }else {
        echo 'user not logged in!';
        }
    }else{
            if ($id) {
                $user_info = get_userdata($id);
                $id = $user_info->ID;
            }

            if(isset($_POST['user_avatar_edit_submit'])){
                do_action('edit_user_profile_update', $id);
            }
            $myAv = new simple_local_avatars($file);
            $myAv->edit_user_profile_update( get_userdata($id));
    }
}
function skip_user_exist($user){
    define( 'WP_IMPORTING', 'SKIP_USER_EXIST' );
    return $user;
}
add_action( 'wp_head', 'mp6_override_toolbar_margin', 11 );
function mp6_override_toolbar_margin() {
  if ( is_admin_bar_showing() ) { ?>
    <style type="text/css" media="screen">
      html { margin-top: 0   !important; }
      * html body { margin-top: 32px !important; }
    </style>
  <?php }
}
function custom_colors() {
   echo '<style type="text/css">
           p.search-box{margin-top:8px;}
         </style>';
}

add_action('admin_head', 'custom_colors');
function director_editable_roles($roles){
    $id = get_current_user_id();
    if($id!=0){
      $user = get_userdata($id);
      $current_roles = $user->roles;
      if(in_array('director', $current_roles)){
          foreach ($roles as $key => $role) {
              if($key != 'candidate' && $key != 'member'){
                unset($roles[$key]);
              }
          }
      }
    }
    return $roles;
}
add_filter('editable_roles', 'director_editable_roles', 100, 3);
function allow_user_to_edit_cpt_filter( $capauser, $capask, $param){

    global $wpdb;

    $company = get_user_meta($param[1], 'company', true);
    $allowed_posts = array( '29', '30' ); // you need to get these ids yourself
    $post = get_post( $param[2] );
    $user = get_userdata( $param[2] );
    $user_company = get_user_meta($user->ID, 'company', true);
    $post_company = get_post_meta($post->ID, '_field_inputtext__1450245186', true);
    $is_director_admin = get_userdata($param[1]);
    if(in_array('manage_own_users', $capauser) && $company && !in_array('administrator', $is_director_admin->roles) && $user->roles){
          if( (( $param[0] == "edit_user") || ( $param[0] == "delete_user" )
            || ( $param[0] == "edit_company_card" ) || ( $param[0] == "delete_company_card" )
            ) && $user_company != $company || in_array('administrator', $user->roles) ) { 
              foreach( (array) $capask as $capasuppr) {
                if ( array_key_exists($capasuppr, $capauser)){
                $capauser[$capasuppr] = 0;
              }
            }
          }
        }
        //var_dump(get_post_meta($post->ID, '_field_inputtext__1450245186'));
         //var_dump($post_company == $company);
        // var_dump($capauser);
        // var_dump($capask);
    if(in_array('manage_company', $capauser) && $company && !in_array('administrator', $is_director_admin->roles)){
          if( (( $capask[0] == "edit_company_card" ) || ( $capask[0] == "delete_company_card" )
            ) && $post_company != $company ) { 
              foreach( (array) $capask as $capasuppr) {
                if ( array_key_exists($capasuppr, $capauser) ) {
                  $capauser[$capasuppr] = 0;
              }
            }
          }
        }
    if(in_array('manage_company', $capauser) && $company && $param[1] != 1){
          if( (( $param[0] == "promote_user" )
            ) && $user_company != $company ) { 
              foreach( (array) $capask as $capasuppr) {
                if ( array_key_exists($capasuppr, $capauser) ) {
                $capauser[$capasuppr] = 0;
              }
            }
          }
        }
    return $capauser;
  }
add_filter('user_has_cap', 'allow_user_to_edit_cpt_filter', 100, 3 );



function my_custom_post_company_card() {
  $labels = array(
    'name'               => _x( 'Компании', 'post type general name' ),
    'singular_name'      => _x( 'Компания', 'post type singular name' ),
    'add_new'            => _x( 'Добавить', 'company' ),
    'add_new_item'       => __( 'Добавить компанию' ),
    'edit_item'          => __( 'Обновить' ),
    'new_item'           => __( 'Новая' ),
    'all_items'          => __( 'Все компании' ),
    'view_item'          => __( 'Просмотреть' ),
    'search_items'       => __( 'Найти компанию' ),
    'not_found'          => __( 'Компании не найдены' ),
    'not_found_in_trash' => __( 'Компании не найдены' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Компании'
  );
  $caps = array(
    'publish_posts' => 'publish_company_cards',
    'creare_posts' => 'create_company_cards',
    'edit_posts' => 'edit_company_cards',
    'edit_others_posts' => 'edit_other_company_cards',
    'delete_posts' => 'delete_company_cards',
    'delete_others_posts' => 'delete_others_company_cards',
    'read_posts'      => 'read_company_cards',
    'read_private_posts' => 'read_private_company_cards',
    'edit_post' => 'edit_company_card',
    'delete_post' => 'delete_company_card',
    'read_post' => 'read_company_card',
    );
  $args = array(
    'labels'        => $labels,
    'description'   => '',
    'capability_type' => 'post',
    'capabilities'  => $caps,
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'company_card', $args ); 
}
add_action( 'init', 'my_custom_post_company_card' );

remove_action( 'wp_version_check', 'wp_version_check' );

remove_action( 'admin_init', '_maybe_update_core' );

add_filter( 'pre_transient_update_core', create_function( '$a', "return null;"));

add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;"));

wp_clear_scheduled_hook( 'wp_version_check' );

remove_action( 'load-plugins.php', 'wp_update_plugins' );

remove_action( 'load-update.php', 'wp_update_plugins' );

remove_action( 'load-update-core.php', 'wp_update_plugins' );

remove_action( 'admin_init', '_maybe_update_plugins' );

remove_action( 'wp_update_plugins', 'wp_update_plugins' );

add_filter( 'pre_transient_update_plugins', create_function( '$a', "return null;" ) );

add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

wp_clear_scheduled_hook( 'wp_update_plugins' );

remove_action( 'load-themes.php', 'wp_update_themes' );

remove_action( 'load-update.php', 'wp_update_themes' );

remove_action( 'load-update-core.php', 'wp_update_themes' );

remove_action( 'admin_init', '_maybe_update_themes' );

remove_action( 'wp_update_themes', 'wp_update_themes' );

add_filter( 'pre_transient_update_themes', create_function( '$a', "return null;" ) );

add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );

wp_clear_scheduled_hook( 'wp_update_themes' );

function clear_dash(){
  global $wp_meta_boxes;
  //var_dump($wp_meta_boxes['dashboard']['normal']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // "Плагины"
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // "Other WordPress News"
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // "WordPress Blog"
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // "Свежие черновики"
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // "Быстрая публикация"
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // "Входящие ссылки"
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // "Свежие комментарии"
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // "Прямо сейчас"
  remove_action('welcome_panel', 'wp_welcome_panel'); // "Добро пожаловать"
}
add_action('wp_dashboard_setup', 'clear_dash' );
?>