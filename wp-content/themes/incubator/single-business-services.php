<?php get_header()?>
    <section>
        <div class="border">
        </div>
        <div class="col-8 sp">
            <div class="wrap">
                <div class="col-1 main" id="main">
                    <p class="head1"><?php echo get_post_meta($post->ID, 'Title', true); ?></p>   
                            <?php $content = get_the_content();
                            $res = array();
                            $res = parse_prev($content);
                            $img = $res['img'];
                            $text = $res['text'];
                            $title = $res['title'];
                            $link = $res['link'];
                            ?>
                            <div class="main_block">
                                <div class="<?php if ( has_post_thumbnail()) {?>img_thumbnail<?php } ?>">
                                    <?php echo the_post_thumbnail();?>
                                </div>
                                <div class="block_text <?php if ( has_post_thumbnail()) {?>has_thumbnail<?php } ?>">
                                    <?php echo $title;?>
                                    <?php echo $text;?>
                                    <?php echo $link; ?>
                                    <p class="news_open">
                                        Подробней
                                    </p>
                                </div>        
                            </div>
                        <?php endwhile;?>
                    <?php endif;?>
                    <div class="pagination">
                        <!--  img class="reversed" - повернутые стрелки -->
                        <div class="pg_ar_prev">
                            <img class="pg_prev" src="<?php echo get_template_directory_uri() . "/img/pg_arrow_disabled.png" ?>">
                            <a href="" class="pg_a disabled">Пердыдущая</a>
                        </div>
                        
                        <a href="#">
                            <p class="nomargin now">1</p>
                        </a>
                        <a href="#">
                            <p class="nomargin">2</p>
                        </a>
                        <a href="#">
                            <p class="nomargin">3</p>
                        </a>
                        
                        <p class="nomargin">...</p>
                        <a href="#">
                            <p class="nomargin pg_last">34</p>
                        </a>
                        <div class="pg_ar_next">
                            <a href="#" class="pg_a">Следующая</a>
                            <img class="pg_next" src="<?php echo get_template_directory_uri() . "/img/pg_arrow_enabled.png" ?>">
                        </div>
                    </div>
                </div>
                <?php get_sidebar()?>
            </div>
        </div>
    </section>
    <?php get_footer()?>
