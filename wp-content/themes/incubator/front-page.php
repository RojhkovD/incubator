<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=<?php bloginfo('charset'); ?> name="viewport" content="initial-scale=0.8, user-scalable=no, maximum-scale=1" /> 
	<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo get_template_directory_uri() . "/css/style.min.css"?>"/>
	<script defer src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script defer type="text/javascript" src="<?php echo get_template_directory_uri() . "/js/main.min.js"?>"></script>
	<!--[if lte IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta3)/IE9.js"></script>
	<![endif]-->
</head>
<?php wp_head(); ?>
<body>
	<header id="header1">
	<div class="logo_signin">
		<div class="logo">
			<a href="<?php echo esc_url( get_permalink(36))?>">
				<img src="<?php echo get_template_directory_uri() . "/img/logo.png"?>">
			</a>
		</div>
		<div class="signin">
			<p id="sign_up" class="sign_up">Регистрация</p>
			<p id="sign_in" class="sign_in">Вход</p>
		</div>
	</div>
	<nav>
		<?php wp_nav_menu();?>
	</nav>
	<?php get_template_part('registration'); ?>
	<?php get_template_part('request_registration'); ?>

	<article class="black_stripe">
		<p>ПРОГРАММА РАЗВИТИЯ МАЛОГО И СРЕДНЕГО БИЗНЕСА ЕБРР В РОССИИ СТАЛА БОЛЕЕ...</p>
		<div class="wrapper">
			<a href="#" class="btn">Подробней</a>
		</div>
	</article>
	</header>
	<section>
		<div class="border"></div>
		<div class="col-8 sp">
			<div class="wrap">
				<div class="col-1 main front_main" id="main">
					<?php if (have_posts()): while (have_posts()): the_post(); ?>
					<p class="head1"><?php echo get_post_meta($post->ID, 'Title', true); ?></p>
					<p><?php the_content(); ?></p>
					<?php endwhile; endif; ?>
					<p>В Академпарке реализована система специализированных бизнес-инкубаторов, таким образом Инкубатор технологий Академпарка на сегодняшний день объединяет в себе <span style="font-weight:600;">четыре направления:</span></p>
				<figure id="bio">
					<a href="#">
					<div class="square" id="square1"></div>
					<div class="fi">
						<img src="<?php echo get_template_directory_uri() . "/img/bio.png"?>">
	                </div>
					<p class="section">БИОТЕХНОЛОГИИ</p>
					</a>
				</figure>
				<figure id="prom">
					<a href="#">
					<div class="square" id="square2"></div>
					<div class="fi">
	                    <img id="mark" src="<?php echo get_template_directory_uri() . "/img/prom.png"?>">
	                </div>
					<p class="section">ПРИБОРОСТРОЕНИЕ</p>
					</a>
				</figure>
				<figure id="it">
					<a href="#">
					<div class="square" id="square3"></div>
					<div class="fi">
	                    <img src="<?php echo get_template_directory_uri() . "/img/it.png"?>">
	                </div>
					<p class="section">IT ТЕХНОЛОГИИ</p>
					</a>
				</figure>
				<figure id="nano">
					<a href="#">
					<div class="square" id="square4"></div>
					<div class="fi">
	                    <img src="<?php echo get_template_directory_uri() . "/img/nano.png"?>">
	                </div>
					<p class="section">НАНОТЕХНОЛОГИИ</p>
					</a>
				</figure>
				<p>Ознакомиться с полным списком услуг, которые оказывает Инкубатор технологий Академпарка своим резидентам, Вы можете в соответствующих разделах.</p>
				</div>
				<?php get_sidebar()?>
			</div>
		</div>
	</section>
	<?php get_footer()?>
