Event = (function() {

  var guid = 0
    
  function fixEvent(event) {
	event = event || window.event
  
    if ( event.isFixed ) {
      return event
    }
    event.isFixed = true 
  
    event.preventDefault = event.preventDefault || function(){this.returnValue = false}
    event.stopPropagation = event.stopPropagaton || function(){this.cancelBubble = true}
    
    if (!event.target) {
        event.target = event.srcElement
    }
  
    if (!event.relatedTarget && event.fromElement) {
        event.relatedTarget = event.fromElement == event.target ? event.toElement : event.fromElement;
    }
  
    if ( event.pageX == null && event.clientX != null ) {
        var html = document.documentElement, body = document.body;
        event.pageX = event.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
        event.pageY = event.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
    }
  
    if ( !event.which && event.button ) {
        event.which = (event.button & 1 ? 1 : ( event.button & 2 ? 3 : ( event.button & 4 ? 2 : 0 ) ));
    }
	
	return event
  }  
  
  /* Вызывается в контексте элемента всегда this = element */
  function commonHandle(event) {
    event = fixEvent(event)
    
    var handlers = this.events[event.type]

	for ( var g in handlers ) {
      var handler = handlers[g]

      var ret = handler.call(this, event)
      if ( ret === false ) {
          event.preventDefault()
          event.stopPropagation()
      }
    }
  }
  
  return {
    add: function(elem, type, handler) {
      if (elem.setInterval && ( elem != window && !elem.frameElement ) ) {
        elem = window;
      }
      if (!handler.guid) {
        handler.guid = ++guid
      }
      
      if (!elem.events) {
        elem.events = {}
		elem.handle = function(event) {
		  if (typeof Event !== "undefined") {
			return commonHandle.call(elem, event)
		  }
        }
      }
	  
      if (!elem.events[type]) {
        elem.events[type] = {}        
      
        if (elem.addEventListener)
		  elem.addEventListener(type, elem.handle, false)
		else if (elem.attachEvent)
          elem.attachEvent("on" + type, elem.handle)
      }
      
      elem.events[type][handler.guid] = handler
    },
    
    remove: function(elem, type, handler) {
      var handlers = elem.events && elem.events[type]
      
      if (!handlers) return
      
      delete handlers[handler.guid]
      
      for(var any in handlers) return 
	  if (elem.removeEventListener)
		elem.removeEventListener(type, elem.handle, false)
	  else if (elem.detachEvent)
		elem.detachEvent("on" + type, elem.handle)
		
	  delete elem.events[type]
	
	  
	  for (var any in elem.events) return
	  try {
	    delete elem.handle
	    delete elem.events 
	  } catch(e) { // IE
	    elem.removeAttribute("handle")
	    elem.removeAttribute("events")
	  }
    } 
  }
}())
// function asideWidth(){
// 	if(document.getElementById('sidebar').offsetHeight > document.getElementById('main').offsetHeight){
// 		var height = document.getElementById('sidebar').offsetHeight;
// 		document.getElementById('main').style.height = height + "px";
// 	}else{
// 		var height = document.getElementById('main').offsetHeight;
// 		document.getElementById('sidebar').style.height = height  + "px";
// 	}
// }
// var y = document.getElementById("header1");
// Event.add(window, 'load', function(){
// 	asideWidth();
// });
// Event.add(window, 'resize', function(e){
// 	asideWidth();	
// });
var d = new Date();
var month = d.getMonth()+1;
var day = d.getDate();
var output = d.getFullYear() + '-' +
	(month<10 ? '0' : '') + month + '-' +
	(day<10 ? '0' : '') + day;
$('#date').DatePicker({
	flat: true,
	date: output,
	current: output,
	calendars: 1,
	starts: 1
});

function assignBorder(){
	if(document.getElementById('sidebar').offsetHeight > document.getElementById('main').offsetHeight){
		document.getElementById('main').style.borderRight = "none";
		document.getElementById('sidebar').style.borderLeft = "1px solid #e0e7ee";
	}else{
		document.getElementById('main').style.borderRight = "1px solid #e0e7ee";
		document.getElementById('sidebar').style.borderLeft = "none";
	}
}
Event.add(window, 'resize', assignBorder);

function changePlaceholders(){
	var t = [];
	var g = [];
	var placeholders = []; 
	function count(element, index, array) {
  		g[index] = element;
  		placeholders[index] = element.placeholder;
	}	
	var o = ["d", "e", "r"];
	var t = document.getElementsByClassName('text_input');
	[].forEach.call(t, count);
	for(var k = 0;k<g.length;k++){
		Event.add(t[k], 'focus', function(e){
			e.target.placeholder = "";
		});
		var l = placeholders[k];
		Event.add(t[k], 'blur', function(l){
			return function(e){
				e.target.placeholder = l;
			}
		}(l));
	}
}
changePlaceholders();

function reg_log(){
	var reg_button = document.getElementById('sign_up');
	var log_button = document.getElementById('sign_in');
	var wrapper = document.getElementById('fade');
	var sign_in = document.getElementById('form_sign_in');
	var sign_up = document.getElementById('form_sign_up');
	Event.add(reg_button, 'click', function(wrapper, sign_up){
		return function(){
			wrapper.style.display = "block";
			sign_up.style.display = "block";
			var cross = document.getElementById('cross2');
			Event.add(cross, 'click', function(wrapper, sign_up){
								return function(){
									wrapper.style.display = "none";
									sign_up.style.display = "none";
								}
			}(wrapper, sign_up));
		}
	}(wrapper, sign_up));
	Event.add(log_button, 'click', function(wrapper, sign_in){
		return function(){
			wrapper.style.display = "block";
			sign_in.style.display = "block";
			var cross = document.getElementById('cross1');
			Event.add(cross, 'click', function(wrapper, sign_in){
								return function(){
									wrapper.style.display = "none";
									sign_in.style.display = "none";
								}
			}(wrapper, sign_in));
		}
	}(wrapper, sign_in));
}
reg_log();



document.getElementById('upload_file').onchange = function(){
	return function(e) {
	    var f = e.target.files[0];
	    if (!f.type.match('image.*')){
	    	console.log("not image");
	    	return 0;
		}
		else{
	    	var fr = new FileReader();
	      		fr.onload = (function(theFile) {
	        		return function(e) {
	          			var img = document.createElement('img');
	          			img.src = e.target.result;
	          			var h = document.getElementById('upload_img').getElementsByTagName('img')[0];
	          			if(h){
	          				document.getElementById('upload_img').removeChild(h);
	          			}
	          			var last = document.getElementById('upload_text');

	          			document.getElementById('upload_img').insertBefore(img, last);
	        	};
	      	})(f);
	    	fr.readAsDataURL(f);
	    }
	};
}();
document.getElementById('upload_text').onclick = function(){
	return function(e){
		var upload_box = document.getElementById('upload_file');
		upload_file.click();
	}
}();
function today(){
	var elem = document.getElementsByClassName('datepickerDays')[0].getElementsByClassName('datepickerDays');
	var span = elem.getElementsByTagName('a')[0].getElementsByTagName('span')[0];
	elem.getElementsByTagName('a')[0].removeChild(span);
	var today = document.createElement('div');
	span.className="calendar_date";
	today.appendChild(span);
	today.className = "calendar_mark";
	elem.getElementsByTagName('a')[0].appendChild(today);
}
Event.add(document.getElementsByClassName('datepickerDays')[0].getElementsByClassName('datepickerDays'), 'click', today); 
function updateCalendar(e){
	console.log("START");
	var elem = e.currentTarget;
	console.log(elem);
	var span = elem.getElementsByTagName('a')[0].getElementsByTagName('span')[0];
	console.log(span);
	//setTimeout(function(){return elem.getElementsByTagName('a')[0].removeChild(span);}, 1000);
	var today = document.createElement('div');
	span.className="calendar_date";
	today.appendChild(span);
	today.className = "calendar_mark";
	elem.getElementsByTagName('a')[0].appendChild(today);
	}
	for(var p = 0;p<document.getElementsByClassName('datepickerDays')[0].getElementsByTagName('td').length;p++){
		Event.add(document.getElementsByClassName('datepickerDays')[0].getElementsByTagName('td')[p], 'click', function(e){
			alert("SDC");
	    	setTimeout( updateCalendar(e), 1);
		});
	}