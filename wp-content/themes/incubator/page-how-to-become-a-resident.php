<?php get_header()?>
    <section>
    <?php global $page_id;
    $page_id = 71;  ?>
        <div class="border">
        </div>
        <div class="col-8 sp">
            <div class="wrap">
                <div class="col-1 main" id="main">
                    <p class="head1"><?php echo get_post_meta($post->ID, 'Title', true); ?></p>
                        <?php if (have_posts()): 
                        while (have_posts()): the_post(); ?>
                            <?php if ( get_the_content() != ""):?>
                            <div class="main_block">
                                <div class="<?php if ( has_post_thumbnail()) {?>img_thumbnail<?php } ?>">
                                <?php echo the_post_thumbnail('small-thumbnail');?>
                                </div>
                                <div class="block_text <?php if ( has_post_thumbnail()) {?>has_thumbnail<?php } ?>">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <?php endif;?>
                        <?php endwhile;?>
                    <?php endif;?>
                </div>
                <?php get_sidebar()?>
            </div>
        </div>
    </section>
    <?php get_footer()?>

