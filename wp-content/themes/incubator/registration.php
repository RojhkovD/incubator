<?php
/* Template Name: registration */
?>
<div class="fixed">
	<div id="fade" class="fade">
	</div>
	<div id="form_sign_in" class="form_sign_in">
		<p id="cross1" class="x">
		</p>
		<p class="head3">
			ВХОД
		</p>
		<form action="<?php echo wp_login_url()?>" method="post">
			<input type="text" placeholder="Логин или e-mail" class="text_input" name="log" id="user_login" aria-describedby="login_error" class="input" value="" size="20">
			<input type="password" placeholder="Пароль" class="text_input" name="pwd" id="user_pass" aria-describedby="login_error" class="input" value="" size="20">
			<input type="submit" value="ВОЙТИ" class="big_green_button center">
		</form>
	</div>
	<div id="form_sign_up" class="form_sign_up">
		<p id="cross2" class="x">
		</p>
		<p class="head3">
			РЕГИСТРАЦИЯ
		</p>
		<form action="<?php the_permalink(); ?>" method="post" enctype="multipart/form-data">
			<input class="text_input" type="text" placeholder="Ваше имя" name="username">
			<input class="text_input" type="email" placeholder="Ваш e-mail" name="email">
			<input class="text_input" type="text" placeholder="Название компании" name="company">
			<input class="text_input" type="tel" placeholder="Ваш телефон" name="phone">
			<input type="hidden" name="reg" value="true">
			<div id="upload_img">
				<img id="blank_img" src="#">
				<input type="file" name="upload_file" id="upload_file" accept="image/*">
				<?php wp_nonce_field( plugin_basename( __FILE__ ), 'example-jpg-nonce' ); ?>
				<div id="upload_text">
					Загрузить фото
				</div>
			</div>
			<div class="representative_checkbox">
				<input id="representative" type="checkbox" value="rep" name="representative">
				<label for="representative">Я представитель компании</label>
			</div>
			<input type="submit" value="ОТПРАВИТЬ ЗАПРОС" class="big_green_button center">
		</form>
	</div>
</div>	