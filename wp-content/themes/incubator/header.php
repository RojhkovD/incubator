<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 */
 	
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=<?php bloginfo('charset'); ?> name="viewport" content="initial-scale=0.8, user-scalable=no, maximum-scale=1" /> 
	<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo get_template_directory_uri() . "/css/style.min.css"?>"/>
	<script defer src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script defer type="text/javascript" src="<?php echo get_template_directory_uri() . "/js/main.min.js"?>"></script>
	<!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta3)/IE9.js"></script>
	<![endif]-->
</head>
<body>
	<header id="header1" style="background-image:none; height:0px;">
		<div class="logo_signin">
			<div class="logo">
				<a href="<?php echo esc_url( get_permalink(36))?>">
					<img src="<?php echo get_template_directory_uri() . "/img/logo.png"?>">
				</a>
			</div>
			<div class="signin">
				<p id="sign_up" class="sign_up">Регистрация</p>
				<p id="sign_in" class="sign_in">Вход</p>
			</div>
		</div>
		<nav>
			<?php 
			$id = get_the_ID();
			$defaults = array(
				'echo'      => 0,
			);
			$menu = wp_nav_menu($defaults);
			if ($id == 142 || $id == 144 || $id == 146 || $id == 162 || $id == 164 || $id == 166 || $id == 168)
			{
				$position = strpos($menu, "170") + 12;
				$menu = substr_replace($menu, "parent_menu ", $position, 0);
				echo $menu;
			}
			if ($id == 38 || $id == 102)
			{
				$position = strpos($menu, "48") + 11;
				$menu = substr_replace($menu, "parent_menu ", $position, 0);
				echo $menu;
			}
			if ($id == 65 || $id == 140 || $id == 69 || $id == 71)
			{
				$position = strpos($menu, "87") + 11;
				$menu = substr_replace($menu, "parent_menu ", $position, 0);
				echo $menu;
			}
			if ($id == 92 || $id == 94)
			{
				$position = strpos($menu, "122") + 12;
				$menu = substr_replace($menu, "parent_menu ", $position, 0);
				echo $menu;
			}
			if ($id == 96 || $id == 98 || $id == 100)
			{
				$position = strpos($menu, "124") + 12;
				$menu = substr_replace($menu, "parent_menu ", $position, 0);
				echo $menu;
			}	
			?>
		</nav>
		<?php wp_head(); ?>
		<?php get_template_part('registration');?>
		<?php get_template_part('request_registration');?>