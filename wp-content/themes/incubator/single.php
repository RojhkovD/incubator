<?php get_header(); ?>
	<section>
		<div class="border">
		</div>
			<div class="col-8 sp">
				<div class="wrap">
					<div class="col-1 main news_main" id="main">
						<?php if (have_posts()): while (have_posts()): the_post(); ?>
						<p class="head1"><?php the_title(); ?></p>
						<p><?php the_content(); ?></p>
						<?php endwhile; endif; ?>
					</div>
					<?php get_sidebar()?>
				</div>
			</div>
	</section>
<?php get_footer(); ?>