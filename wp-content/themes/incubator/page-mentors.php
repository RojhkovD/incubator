<?php get_header()?>
    <section>
    <?php global $page_id;
    $page_id = 166;  

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $args = array(
          'posts_per_page' => 1,
          'paged'          => $paged,
          'cat'            => 14
        );
    $the_query = new WP_Query( $args );?>
        <div class="border">
        </div>
        <div class="col-8 sp">
            <div class="wrap">
                <div class="col-1 main" id="main">
                    <p class="head1"><?php echo get_post_meta($post->ID, 'Title', true); ?></p>
                    <?php if (have_posts()): 
                        while (have_posts()): the_post(); ?>
                            <?php if ( get_the_content() != ""):?>
                            <div class="main_block">
                                <div class="<?php if ( has_post_thumbnail()) {?>img_thumbnail<?php } ?>">
                                <?php echo the_post_thumbnail('small-thumbnail');?>
                                </div>
                                <div class="block_text <?php if ( has_post_thumbnail()) {?>has_thumbnail<?php } ?>">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <?php endif;?>
                        <?php endwhile;?>
                    <?php endif;?>      
                    <?php if ($the_query->have_posts()): 

                         while ($the_query->have_posts()): $the_query->the_post();?>
                            <?php $content = get_the_content();
                            $res = array();
                            $res = parse_prev($content);
                            $img = $res['img'];
                            $text = $res['text'];
                            $title = $res['title'];
                            $link = $res['link'];
                            ?>
                            <div class="main_block">
                                <div class="<?php if ( has_post_thumbnail()) {?>img_thumbnail<?php } ?>">
                                    <?php echo the_post_thumbnail('small-thumbnail');?>
                                </div>
                                <div class="block_text <?php if ( has_post_thumbnail()) {?>has_thumbnail<?php } ?>">
                                    <?php echo $title;?>
                                    <?php echo $text;?>
                                    <?php echo $link; ?>
                                    <p class="news_open">
                                        Подробней
                                    </p>
                                </div>        
                            </div>
                        <?php endwhile;?>
                    <?php endif;?>
                    <?php $args = [
                                'query'                 => $the_query,
                                'previous_page_text'    => __( 'Предыдущая' ),
                                'next_page_text'        => __( 'Следующая' ),
                                'first_page_text'       => __( 'First' ),
                                'last_page_text'        => __( 'Last' ),
                                'next_link_text'        => __( 'Older Entries' ),
                                'previous_link_text'    => __( 'Newer Entries' ),
                                'show_posts_links'      => false,
                                'range'                 => 2,
                            ];
                        
                       echo get_paginated_numbers($args);?>
                </div>
                <?php get_sidebar()?>
            </div>
        </div>
    </section>
    <?php get_footer()?>
